import Vue from 'vue';
import Vuex from 'vuex';

/**
 *  Use the statement below to import all/any requirements as needed
 *  including custom store modules :)
 */
// import * as user from './modules/user';

Vue.use(Vuex);

export default new Vuex.Store({
    actions: {},
    getters: {},
    mutations: {
        SET_NAVIGATION_URL(state, navigationUrl) {
            state.navigationUrl = navigationUrl;
        }
    },
    state: {
        navigationUrl: ''
    },
});
