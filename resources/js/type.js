import Typed from 'typed.js';

let options = {
    strings: ['Welcome!', 'I design cool things!', 'Full-Stack Developer'],
    typeSpeed: 50,
    showCursor: false,
};

if (window.location.pathname === '/') {
    let typed = new Typed('.typed-title', options);
}
