require('./bootstrap');

window.Vue = require('vue');

/**
 *
 */
import store from './store';

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */
Vue.component('NavigationBar', require('./components/navigation/NavigationBar').default);
Vue.component('NavItem', require('./components/navigation/NavItem').default);
Vue.component('FooterNavigation', require('./components/navigation/FooterNavigation').default)

Vue.component('Projects', require('./components/projects/Projects').default);

const app = new Vue({
    el: '#app',
    store,
});
