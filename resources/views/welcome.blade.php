@extends('layouts.app')

@section('content')
    <div class="position-ref full-height">
        <div class="content mt-15">
            <div class="title">
                <p class="typed-title text-5xl h-16"></p>
            </div>

            <div class="links text-xl lg:pt-4 sm:pt-2">
                <h2>Freelance Developer + Designer</h2>
            </div>
        </div>
    </div>
@endsection
