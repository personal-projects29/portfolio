const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const tailwindCss = require('tailwindcss');

mix.js('resources/js/*.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css').options({
        processCssUrls: false,
        postCss: [ tailwindCss('./tailwind.config.js') ]
    });
